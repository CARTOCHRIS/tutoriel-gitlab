Tutoriel d'utilisation de OSRM (Open Source Routing Machine)
===========================

Sommaire:
- Qu'est ce qu'OSRM?
- Utilisation via R
- Utilisation via python

Qu'est ce qu'OSRM
------------------

OSRM signifie Open Source Routing Machine soit moteur de recherche d'itinéraire 
routier à code source ouvert. OSRM vise ainsi à obtenir le plus court chemin dans un 
réseau routier entre deux points et vient avec un ensemble de fonctionalités autour 
de ce principe de base. OSRM est implémenté en C++ et basé 
sur le réseau 
routier du projet open source OpenStreeMap (OSM). OSRM fournit un service web 
permettant d'effectuer des requêtes OSRM via une API. Ainsi tout outil permettant 
des requêtes HTTP peut requêter OSRM. Il existe des projets en R et en python 
facilitant l'interaction avec OSRM. 

De façon simplifiée on va demander la distance entre deux points géographique et 
OSRM 
va renvoyer un itinéraire pour faire ce chemin ainsi que la distance et le temps 
estimé pour effectuer ce chemin.

Il est possible d'utiliser le serveur de demo OSRM pour des requêtes 
[ici par exemple](http://map.project-osrm.org/). Cependant pour 
une utilisation plus importante il est conseillé de déployer son propre serveur 
OSRM.

Une instance d'OSRM est installée au sein de la DREES avec un accès libre à tout 
agent voulant profiter de ce service. L'IP du serveur: 10.200.15.24:5000

Voici la documentation (en anglais) pour l'intérogation de OSRM via HTTP: 
[ici](https://github.com/Project-OSRM/osrm-backend/blob/master/docs/http.md)

Utilisation via R
----------------

Il existe une bibliothèque R permettant d'effectuer des requêtes à OSRM. La 
bibliothèque se nomme [osrm](https://github.com/rCarto/osrm).

[Ici](https://gitlab.com/DREES/tutoriels/blob/master/exemples/osrm/osrm_R.R) un exemple de script R pour 
utiliser 
le package.

Utilisation via python
---------------------

Il y a plusieurs bibliothèques permettant l'utilisation d'OSRM. La bibliothque 
privilégiée est [celle-ci](https://github.com/ustroetz/python-osrm).

[Ici](https://gitlab.com/DREES/tutoriels/blob/master/exemples/osrm/osrm_python.ipynb) 
un exemple de notebook 
pour 
utiliser le package.
